import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { Activites } from './components/Activités'


 

function App() {
  
  const [count, setCount] = useState(0);
  
 const decrementer = () => {
  setCount((count) => (count > 0 ? count - 1 : 0));
};
  
  return (
    <>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          Incrementer le compteur
        </button>
        <button onClick ={decrementer} > Décrementer le compteur

        </button>

        <button onClick ={() => setCount ((count) => 0)} > Remise à zéro du compteur
        </button>
        <p>Nombre de participants : {count}</p>
      </div>
    </>
  );
}

export default App;
