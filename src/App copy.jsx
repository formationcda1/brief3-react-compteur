import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { Activites } from './components/Activités'


// const activitesList = [
//   {
//     id:0,
//     name: "Match", 
//     image:"", text:"Venez participer au match" , 
//     button:"",
//     participants : "",
//   },
//   {
//      id:2,
//     name: "Festival",
//      image:"",
//       text:"Venez participer au festival" , 
//       button:"",
//       participants : "",
//   },
//   {
//     id:3,
//     name: "Cinéma", 
//     image:"", 
//     text:"Venez voir un film" , 
//     button:"",
//     participants : "",
    
//   },
//   { id:4,
//     name: "Randonnée",
//      image:"",
//       text:"Venez participer à une super rando !" ,
//        button:"",
//        participants : "",
//   },
// ]
  

function App() {
  // const [count, setCount] = useState(0)
  const [activites, setActivites] = useState([
    { id: 0, nom: "Match", image: "https://cdn-s-www.lalsace.fr/images/59553175-DCC4-4C20-A0C6-0D561FF3AE85/NW_raw/le-match-france-senegal-pendant-la-coupe-du-monde-de-mulhouse-au-drouot-ce-samedi-photo-l-alsace-jean-francois-frey-1624721959.jpg", texte: "Venez participer au match", participants: 6 },
    { id: 1, nom: "Festival", image: "https://offloadmedia.feverup.com/toulousesecret.com/wp-content/uploads/2021/05/14065304/IMG_5527-Fête-de-la-musique-2015-à-Toulouse.png", texte: "Venez participer au festival", participants: 9 },
    { id: 2, nom: "Cinéma", image: "https://cloudfront-eu-central-1.images.arcpublishing.com/leparisien/QM3QPNKE6SRBMXPOEIPWN7WZFY.jpg", texte: "Venez voir un film", participants: 8 },
    { id: 3, nom: "Randonnée", image: "https://demarchesadministratives.fr/images/demarches/290/Organiser-randonnee.jpg", texte: "Venez participer à une super rando !", participants: 15 }
  ]);
  
  
  return (
    <>
<div>
<Activites
          key={activite.id}
          image={activite.image}
          nom={activite.nom}
          texte={activite.texte}
          participants={activite.participants}
          Incrementer={() => handleIncrementer(activite.id)}
          Decrementer={() => handleDecrementer(activite.id)}
        />

</div>
 

    </>
  )
}  

{/* 
const [count, setCount] = useState(0)
<div className="card">

        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>

      </div>  */}
      
export default App
 